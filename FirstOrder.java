/*
 * Copyright (C) 2019  Babita Sharma and Franck van Breugel
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version
.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You can find a copy of the GNU General Public License at
 * <http://www.gnu.org/licenses/>.
 */

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintStream;
import java.util.InputMismatchException;
import java.util.NoSuchElementException;
import java.util.Scanner;

/**
 * An implementation of an algorithm that approximates the probabilistic bisimilarity distances of a 
 * labelled Markov chain.  The algorithm is described in the following paper:
 * Franck van Breugel, Babita Sharma and James Worrell.
 * Approximating a Behavioural Pseudometric without Discount for Probabilistic Systems. 
 * Logical Methods in Computer Science, 4(2), 2008.
 * <p>
 * The app takes two command line arguments.  The first one is the base name of the files that describe
 * the labelled Markov chain.  For example, if the base name is sample then two files need to be provided: 
 * sample.tra and sample.lab.  The file sample.tra contains the transitions and their probabilities. 
 * For example,
<pre>
3 4
0 1 0.5
0 2 0.5
1 1 1.0
2 2 1.0
</pre>
 * described a labelled Markov transition with three states (first integer) and four transitions
 * (second integer).  The remaining four lines describe the transitions.  The second line describes
 * a transition from state 0 (soutce) to state 1 (target) with probability 0.5.
 * The file sample.lab contains the state labelling of the labelled Markov chain.  For example, 
<pre>
0 0 1
</pre>
 * specifies that state 0 and 1 have the same label (0) and state 2 has another label (1).
 * The second command argument is the desired accuracy of the approximation of the probabilistic
 * bisimilarity distances.  The accuracy should be at least 0.000000000000001.
 * <p>
 * The app writes the distances to the file sample.dis. Since the distance from a state
 * to itself is zero and distances are symmetric, that is, the distance from state s 
 * to state t is equal to the distance from t to s, the file only contains the distance
 * from s to t if s < t.  The first line contains the distances for s = 0, the second 
 * one for s = 1, etc.  For example, when we run the app with the above specified
 * labelled Markov chain and accuracy 0.01, then the file sample.dis has the following content. 
<pre>
0.51	1.00	
1.00	

</pre>
 * It captures that the distance between state 0 and state 1 is in the interval  [0.50, 0.51].
 *  
 * @author Babita Sharma
 * @author Franck van Breugel
 */
public class FirstOrder {
	private FirstOrder() {}

	/*
	 * CVC4 command
	 */
	private static final String CVC4 = "/cs/fac/packages/CVC4-1.7/build/bin/cvc4";

	private static int states; // number of states of the labelled Markov chain
	private static double[][] probability; // transition probabilities of the labelled Markov chain
	private static int[] label; // state labelling of the labelled Markov chain
	private static double accuracy; // desired accuracy of the approximation of the  probabilistic bisimilarity distances
	private static double[][] distance; // probabilistic bisimilarity distances of states (only distance[s][t] with s < t are used)
	private static boolean[][] computed; // those distances that have been computed (only computed[s][t] with s < t are used)
	private static double[][] lowerbound; // lower approximation of the distances (only lowerbound[s][t] with s < t are used)
	private static double[][] upperbound; // upper approximation of the distances (only upperbound[s][t] with s < t are used)

	/**
	 * Approximates the probabilistic bisimilarity distances for a labelled Markov chain.
	 *  
	 *  @args[0] the base name of the files describing the labelled Markov chain
	 *  @args[1] the desired accuracy of the approximation
	 */
	public static void main(String[] args) {
		try {
			// read the transition probabilities
			readProbabilities(args[0] + ".tra");

			// read the state labelling
			readLabels(args[0] + ".lab");

			// set distance of states with different labels to one
			setOne();

			// set distance of states that are probabilistic bisimilar to zero
			boolean[][] bisimilar = ProbabilisticBisimilarity.decide(probability, label);
			setZero(bisimilar);

			// set lower- and upperbounds for distances to zero and one, respectively
			setBounds();

			// desired accuracy of the approximation of the distances
			accuracy = Double.parseDouble(args[1]);
			final double MIN_ACCURACY = 0.000000000000001;
			if (accuracy < MIN_ACCURACY) {
				System.out.println("Accuracy is too small");
				System.exit(0);
			}

			// approximate the distances using binary search	
			for (int s = 0; s < states; s++) {
				for (int t = s + 1; t < states; t++) {
					if (!computed[s][t]) {
						// CVC4 expression that captures that the distances form a pseudometric
						String pseudometric = pseudometric();
						// CVC4 expression that captures that the distances are a postfixed point
						String postfixed = postfixed();

						while (upperbound[s][t] - lowerbound[s][t] > accuracy) {
							double middle = (upperbound[s][t] + lowerbound[s][t]) / 2;

							StringBuffer formula = new StringBuffer();
							formula.append("QUERY (EXISTS (");
							for (int u = 0; u < states; u++) {
								for (int v = u + 1; v < states; v++) {
									if (!computed[u][v]) {
										formula.append(d(u, v) + ":REAL, ");
									}
								}
							}
							formula.delete(formula.length() - 2, formula.length());
							formula.append(") : (" + pseudometric + " AND " + postfixed + " AND " + d(s, t) + " < " + toExpr(middle) + "));");

							// write formula to file
							File file = new File("formula.cvc");
							PrintStream output = new PrintStream(file);
							output.println(formula);
							output.close();

							// query the CVC4 solver
							Runtime runtime = Runtime.getRuntime();
							Process process = runtime.exec(CVC4 + " formula.cvc");
							process.waitFor();

							// retrieve result
							Scanner input = new Scanner(process.getInputStream());
							String result = "";
							while (input.hasNextLine()) {
								result += input.nextLine();
							}
							input.close();

							// if CVC4 fails
							if (result.contains("unknown")) {
								System.out.println("CVC4 cannot determine the validity of the following formula.");
								System.out.println(formula);
								System.exit(0);
							}

							// update the lower- or upperbound
							if (result.contains("invalid")) {
								lowerbound[s][t] = middle;
							} else {
								upperbound[s][t] = middle;
							}
						}

						// set distance
						distance[s][t] = upperbound[s][t]; // upperbound should be used for post fixed point
						computed[s][t] = true;
					}
				}
			}

			// print the distances
			printDistances(args[0] + ".dis");

		} catch (ArrayIndexOutOfBoundsException e) {
			System.out.println("Command line argument(s) missing.");
		} catch (NumberFormatException e) {
			System.out.println("Command line argument for the accuracy is not in the proper format.");
		} catch (FileNotFoundException e) {
			System.out.println(e.getMessage());
		} catch (InputMismatchException e) {
			System.out.println(e.getMessage());
		} catch (NoSuchElementException e) {
			System.out.println(e.getMessage());
		} catch (IOException | InterruptedException e) {
			System.out.println("Something went wrong when calling CVC4");
		}
	}

	/**
	 * Reads the transition probabilities from the given file.
	 *
	 * @param file the name of the file
	 * @throws FileNotFoundException if the given file cannot be read
	 * @throws InputMismatchException if the given file contains not the right type of data
	 * @throws NoSuchElementException if the given file contains not enough data
	 */
	private static void readProbabilities(String file) throws FileNotFoundException, InputMismatchException, NoSuchElementException {
		try {
			Scanner input = new Scanner(new File(file));

			states = input.nextInt(); // number of states
			int transitions = input.nextInt(); // number of transitions

			probability = new double[states][states]; // transition probability
			for (int t = 0; t < transitions; t++) {
				int source = input.nextInt();
				int target = input.nextInt();
				probability[source][target] = input.nextDouble();
			}

			input.close();

		} catch (FileNotFoundException e) {
			throw new FileNotFoundException("File " + file + " is not found.");
		} catch (InputMismatchException e) {
			throw new InputMismatchException("File " + file + " contains not the right type of data.");
		} catch (NoSuchElementException e) {
			throw new NoSuchElementException("File " + file + " contains not enough data.");
		}
	}

	/**
	 * Reads the labelling from the given file.
	 *
	 * @param file the name of the file
	 * @throws FileNotFoundException if the given file cannot be read
	 * @throws InputMismatchException if the given file contains not the right type of data
	 * @throws NoSuchElementException if the given file contains not enough data
	 */
	private static void readLabels(String file) throws FileNotFoundException, InputMismatchException, NoSuchElementException {
		try {
			Scanner input = new Scanner(new File(file));

			label = new int[states]; // state labelling
			for (int s = 0; s < states; s++) {
				label[s] = input.nextInt();
			}

			input.close();

		} catch (FileNotFoundException e) {
			throw new FileNotFoundException("File " + file + " is not found.");
		} catch (InputMismatchException e) {
			throw new InputMismatchException("File " + file + " contains not the right type of data.");
		} catch (NoSuchElementException e) {
			throw new NoSuchElementException("File " + file + " contains not enough data.");
		}
	}

	/**
	 * Sets the distance of those states with different labels to one.
	 */
	private static void setOne() {
		distance = new double[states][states]; // probabilistic bisimilarity distances of states (only distance[s][t] with s < t are used)
		computed = new boolean[states][states]; // those distances that have been computed (only computed[s][t] with s < t are used)

		for (int s = 0; s < states; s++) {
			for (int t = s + 1; t < states; t++) {
				if (label[s] != label[t]) {
					distance[s][t] = 1.0;
					computed[s][t] = true;
				} 
			}
		}
	}

	/**
	 * Sets the distance of those states that are equivalent to zero.
	 *
	 * @param equivalent captures which states are equivalent
	 */
	private static void setZero(boolean[][] equivalent) {
		for (int s = 0; s < states; s++) {
			for (int t = s + 1; t < states; t++) {
				if (equivalent[s][t]) {
					distance[s][t] = 0.0;
					computed[s][t] = true;
				}
			} 
		}
	}

	/**
	 * Sets the lower approximation of the distances to zero and the upper approximation of the distances to one.
	 */
	private static void setBounds() {
		lowerbound = new double[states][states];
		upperbound = new double[states][states];
		for (int s = 0; s < states; s++) {
			for (int t = s + 1; t < states; t++) {
				upperbound[s][t] = 1.0;
			}
		}
	}

	/**
	 * Returns the CVC4 expression that captures that the distances forms a 1-bounded pseudometric.
	 * That is, distances are greater than or equal to 0.0, smaller than or equal to 1.0, the distance
	 * of a state to itself is zero, distances are symmetric and satisfy the triangle inequality.
	 * 
	 * @return the CVC4 expression that captures that the distances forms a 1-bounded pseudometric
	 */
	private static String pseudometric() {
		StringBuffer pseudometric = new StringBuffer();
		pseudometric.append("TRUE"); // true

		// 1-bounded
		for (int s = 0; s < states; s++) {
			for (int t = s + 1; t < states; t++) {
				if (!computed[s][t]) {
					pseudometric.append(String.format(" AND %1$s >= 0 AND %1$s <= 1", d(s, t))); // d(s, t) >= 0.0 && d(s, t) <= 1.0
				}
			}
		}

		// since we only consider d(s, t) with s < t, we do not have to represent d(s, s) = 0 and d(s, t) = d(t, s)

		// triangle inequality: d(s, t) <= d(s, u) + d(u, t)
		for (int s = 0; s < states; s++) {
			for (int t = s + 1; t < states; t++) {
				for (int u = 0; u < states; u++) {
					if (s < u && u < t && (!computed[s][t] || !computed[s][u] || !computed[u][t])) {
						pseudometric.append(String.format(" AND %s <= %s + %s", d(s, t),  d(s, u), d(u, t))); // d(s, t) <= d(s, u) + d(u, t)
					}   
				}
			}
		}

		return pseudometric.toString();
	}

	/**
	 * Determines for each pair of states (u, v) if all couplings of the given states s and t assign 
	 * zero to that pair.  It provides a conservative approximation: if false is returned for (u, v) 
	 * then it may still be the case that all couplings of the given states s and t assign zero to 
	 * that pair.
	 * 
	 * @param s a state
	 * @param t a state
	 * @return zero(s)(t)[u][v] is true if all couplings of the states s and t assign zero to (u, v)
	 */
	private static boolean[][] zero(int s, int t) {
		boolean[][] zero = new boolean[states][states];

		for (int u = 0; u < states; u++) {
			for (int v = 0; v < states; v++) {
				if (probability[s][u] == 0.0 || probability[t][v] == 0.0) { 
					zero[u][v] = true;
				}
			}
		}

		if (computed[s][t] && distance[s][t] == 0.0) {
			for (int u = 0; u < states; u++) {
				for (int v = u + 1; v < states; v++) {
					if (computed[u][v] && distance[u][v] != 0.0) { 
						zero[u][v] = true;
						zero[v][u] = true;
					}
				}
			}
		}

		return zero;
	}

	/**
	 * Returns the CVC4 expression that captures that the distances are a post-fixed point.  
	 * 
	 * @return the CVC4 expression that captures that the distances are a post-fixed point
	 */
	private static String postfixed() {
		StringBuffer postfixed = new StringBuffer();
		postfixed.append("TRUE"); // true

		for (int s = 0; s < states; s++) {
			for (int t = s + 1; t < states; t++) {
				if (!computed[s][t] || distance[s][t] != 1.0) { 
					StringBuffer coupling = new StringBuffer();
					coupling.append("TRUE"); // true

					// determine which state pairs of any coupling of s and t are zero
					boolean[][] zero = zero(s, t);

					// 0.0 <= w(u, v) <= 1.0 for all u and v
					for (int u = 0; u < states; u++) {
						for(int v = 0; v < states; v++) { 
							if (!zero[u][v]) {
								coupling.append(String.format(" AND %1$s >= 0 AND %1$s <= 1", w(u, v))); // w(u, v) >= 0.0 && w(u, v) <= 1.0
							}
						}
					}

					// sum : 0 <= v < states : w(u, v) = probability[s][u]
					for (int u = 0; u < states; u++) {
						if (probability[s][u] != 0) { // if probability[s][u] == 0 then zero[u][v] for all v
							StringBuffer sum = new StringBuffer();
							sum.append("0");

							for (int v = 0; v < states; v++) {
								if (!zero[u][v]) { // zero values can be ignore
									sum.append(" + " + w(u, v));
								}
							}

							sum.append(" = " + toExpr(probability[s][u]));
							coupling.append(" AND " + sum.toString());
						}
					}

					// sum : 0 <= u < states : w(u, v) = probability[t][v]
					for (int v = 0; v < states; v++) {
						if (probability[t][v] != 0) { // if probability[t][v] == 0 then zero[u][v] for all u
							StringBuffer sum = new StringBuffer();
							sum.append("0");

							for (int u = 0; u < states; u++) {
								if (!zero[u][v]) { // zero values can be ignored
									sum.append(" + " + w(u, v));
								}
							}

							sum.append(" = " + toExpr(probability[t][v]));
							coupling.append(" AND " + sum.toString());
						}
					}

					// sum : 0 <= u, v < states : d(u, v) * w(u, v) <= d(s, t)
					StringBuffer sum = new StringBuffer();
					sum.append("0");
					for (int u = 0; u < states; u++) {
						for (int v = 0; v < states; v++) {
							if (u != v && !zero[u][v]) { // zero values can be ignored, d(u, u) = 0
								sum.append(String.format(" + %s * %s ", d(u, v), w(u, v))); // d(u, v) * w(u, v)
							}
						}
					}

					sum.append(" <= " + d(s, t));

					// w is a coupling and sum : 0 <= u, v < states : d(u, v) * w(u, v) <= d(s, t)
					coupling.append(" AND " + sum.toString());

					// there exists w : w is a coupling and sum : 0 <= u, v < states : d(u, v) * w(u, v) <= d(s, t)
					StringBuffer exists = new StringBuffer();
					exists.append("(EXISTS (");
					for (int u = 0; u < states; u++) { 
						for (int v = 0; v < states; v++) {
							if (!zero[u][v]) { 
								exists.append(w(u, v) + ":REAL, ");
							}
						} 
					}  
					exists.delete(exists.length() - 2, exists.length());
					exists.append(") : (" + coupling.toString() + ")");


					postfixed.append(" AND " + exists.toString() +")");
				}
			}
		}
		return postfixed.toString();
	}

	/**
	 * Returns the CVC4 expression, restricted to the unit interval, representing the given value.
	 * 
	 * @param value a value
	 * @return the CVC4 expression representing the given value
	 */
	private static String toExpr(double value) {
		if (value <= 0.0) {
			return "0";
		} else if (value >= 1.0) {
			return "1";
		} else {
			int length = String.valueOf(value).length() - 2; // number of digits after .
			long denominator = (long) Math.pow(10, length);
			long numerator = (long) (Math.round(value * denominator));
			return numerator + "/" + denominator;
		}
	}

	/**
	 * Returns the CVC4 expression that represents the distance of the given states.
	 * 
	 * @param s a state
	 * @param t a state
	 * @return the variable or value that represents the distance of the given states
	 */
	private static String d(int s, int t) {
		if (s == t) {
			return "0";
		} else {
			int min = Math.min(s,  t);
			int max = Math.max(s,  t);
			if (!computed[min][max]) {
				return "d_" + min + "_" + max;
			} else {
				return toExpr(distance[min][max]);
			}
		}
	}

	/**
	 * Returns CVC4 expression that represents the coupling at the given states.
	 * 
	 * @param s a state
	 * @param t a state
	 * @return the CVC4 expression that represents the coupling at the given states
	 */
	private static String w(int s, int t) {
		return "w_" + s + "_" + t;
	}

	/**
	 * Prints approximations of probabilistic bisimilarity distances.
	 *
	 * @throws FileNotFoundException if the given file cannot be read
	 */
	private static void printDistances(String file) throws FileNotFoundException {
		try {
			int precision = String.valueOf(accuracy).length() - 2;

			PrintStream output = new PrintStream(new File(file));

			for (int s = 0; s < states; s++) {
				for(int t = s + 1; t < states; t++) {
					output.printf("%." + precision + "f\t", distance[s][t]);
				}
				output.println();
			}

			output.close();
		} catch (FileNotFoundException e) {
			throw new FileNotFoundException("File " + file + " is not found.");
		}
	}
}
