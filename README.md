Overview
--------

An implementation of an algorithm that approximates the probabilistic bisimilarity distances of a labelled Markov chain.  The algorithm is described in the following paper:

Franck van Breugel, Babita Sharma and James Worrell.  Approximating a Behavioural Pseudometric without Discount for Probabilistic Systems.  Logical Methods in Computer Science, 4(2), 2008.

The app takes two command line arguments.  The first one is the base name of the files that describe the labelled Markov chain.  For example, if the base name is sample then two files need to be provided: sample.tra and sample.lab.  The file sample.tra contains the transitions and their probabilities.  For example,

 3 4
 0 1 0.5
 0 2 0.5
 1 1 1.0
 2 2 1.0

described a labelled Markov transition with three states (first integer) and four transitions (second integer).  The remaining four lines describe the transitions.  The second line describes a transition from state 0 (soutce) to state 1 (target) with probability 0.5.  The file sample.lab contains the state labelling of the labelled Markov chain.  For example, 
	  
 0 0 1

specifies that state 0 and 1 have the same label (0) and state 2 has another label (1).  The second command argument is the desired accuracy of the approximation of the probabilistic bisimilarity distances.  The accuracy should be at least 0.000000000000001.

The app writes the distances to the file sample.dis. Since the distance from a state to itself is zero and distances are symmetric, that is, the distance from state s to state t is equal to the distance from t to s, the file only contains the distance from s to t if s < t.  The first line contains the distances for s = 0, the second one for s = 1, etc.  For example, when we run the app with the above specified labelled Markov chain and accuracy 0.01, then the file sampel.dis has the following content. 

 0.51	1.00	
 1.00	
 

It captures that the distance between state 0 and state 1 is in the interval  [0.50, 0.51].

To run the app, CVC4 needs to be installed and the attribute CVC4 in the class FirstOrder needs to be set to the path to the cvc4 command.

Licensing
---------

This extension is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This extension is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You can find a copy of the GNU General Public License at
http://www.gnu.org/licenses

Questions/comments/suggestions
------------------------------

Please email them to franck@eecs.yorku.ca

Thanks
------

to Babita Sharma who developed an earlier version of the code that used Mathematica and Qiyi Tang who wrote the code to decide probabilitic bisimilarity.
