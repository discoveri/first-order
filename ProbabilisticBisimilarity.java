/*
 * Copyright (C) 2019  Qiyi Tang
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You can find a copy of the GNU General Public License at
 * <http://www.gnu.org/licenses/>.
 */

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

public class ProbabilisticBisimilarity {

	/**
	 * Implements a top-down splay tree. Modified from
	 * http://www.link.cs.cmu.edu/splay/ 
	 * This code is in the public domain.
	 *
	 * @author Danny Sleator <sleator@cs.cmu.edu> 
	 * @author Qiyi Tang
	 */
	private static class BinaryNode {
		private Block block; // 
		private Double sum; // 
		private BinaryNode left; // left child
		private BinaryNode right; // right child

		private BinaryNode(Double sum) {
			this.block = new Block();
			this.block.list = new LinkedList<State>();
			this.sum = sum;
			this.left = null;
			this.right = null;
		}
	}

	private static class SplayTree {
		private BinaryNode root;

		public SplayTree() {
			this.root = null;
		}

		/**
		 * Inserts into the tree.
		 * 
		 * @param sum
		 * @param state
		 */
		public void insert(Double sum, State state) {
			if (this.root == null) {
				this.root = new BinaryNode(sum);
				this.root.block.list.add(state);
				state.block = this.root.block;
			} else {
				this.splay(sum);
				int comparison = sum.compareTo(this.root.sum);
				if (comparison == 0) {
					this.root.block.list.add(state);
					state.block = this.root.block;
				} else {
					BinaryNode node = new BinaryNode(sum);
					node.block.list.add(state);
					state.block = node.block;
					if (comparison < 0) {
						node.left = this.root.left;
						node.right = this.root;
						this.root.left = null;
					} else {
						node.right = this.root.right;
						node.left = this.root;
						this.root.right = null;
					}
					this.root = node;
				}
			}
		}

		private static BinaryNode header = new BinaryNode(null); // for splay

		/**
		 * Performs a top-down splay.
		 * 
		 * splay(key) does the splay operation on the given key. If key is in the
		 * tree, then the BinaryNode containing that key becomes the root. If key is
		 * not in the tree, then after the splay, key.root is either the greatest
		 * key < key in the tree, or the least key > key in the tree.
		 */
		private void splay(Double key) {
			BinaryNode l, r, t, y;
			l = r = header;
			t = root;
			header.left = header.right = null;
			for (;;) {
				if (key.compareTo(t.sum) < 0) {
					if (t.left == null)
						break;
					if (key.compareTo(t.left.sum) < 0) {
						y = t.left; /* rotate right */
						t.left = y.right;
						y.right = t;
						t = y;
						if (t.left == null)
							break;
					}
					r.left = t; /* link right */
					r = t;
					t = t.left;
				} else if (key.compareTo(t.sum) > 0) {
					if (t.right == null)
						break;
					if (key.compareTo(t.right.sum) > 0) {
						y = t.right; /* rotate left */
						t.right = y.left;
						y.left = t;
						t = y;
						if (t.right == null)
							break;
					}
					l.right = t; /* link left */
					l = t;
					t = t.right;
				} else {
					break;
				}
			}
			l.right = t.left; /* assemble */
			r.left = t.right;
			t.left = header.right;
			t.right = header.left;
			root = t;
		}
	}

	private static class Block {
		private List<State> list;
		private SplayTree tree;

		Block() {
			this.list = new LinkedList<State>();
			this.tree = new SplayTree();
		}
	}

	private static class State {
		private Block block;
		private double sum;
		private int index;

		State(int index) {
			this.block = new Block();
			this.sum = 0;
			this.index = index;
		}
	}

	public void lump(List<Block> partition, double[][] probability) {
		LinkedList<Block> splitters = new LinkedList<Block>();
		for (Block block : partition) {
			Block splitter = new Block();
			for (State state : block.list) {
				splitter.list.add(state);
			}
			splitters.add(splitter);
		}
		while (!splitters.isEmpty()) {
			Block splitter = splitters.removeFirst();
			if (splitter.list.size() > 0) {
				split(splitter, partition, splitters, probability);
			}
		}
	}

	public void split(Block splitter, List<Block> partition, List<Block> splitters, double[][] probability) {
		List<State> hasTrans = new LinkedList<State>();
		List<Block> hasSplit = new LinkedList<Block>();

		for (State target : splitter.list) {
			for (Block block : partition) {
				for (State source : block.list) {
					if (probability[source.index][target.index] > 0) {
						source.sum = 0;
						if (!hasTrans.contains(source))
							hasTrans.add(source);
					}
				}
			}
		}

		// update sum to splitter
		for (State target : splitter.list) {
			for (State source : hasTrans) {
				source.sum += probability[source.index][target.index];
			}
		}

		// split splay tree
		for (State source : hasTrans) {
			Block block = source.block;
			block.list.remove(source);
			block.tree.insert(source.sum, source);
			if (!hasSplit.contains(block)) {
				hasSplit.add(block);
			}
		}

		for (Block block : hasSplit) {
			if (block.list.size() == 0) {
				partition.remove(block);
			}
			LinkedList<BinaryNode> queue = new LinkedList<BinaryNode>();
			queue.add(block.tree.root);

			int maxSize = -1;
			Block maxBlock = null;

			while (!queue.isEmpty()) {
				BinaryNode node = queue.removeFirst();
				partition.add(node.block);
				Block newSp = new Block();
				for (State st : node.block.list) {
					newSp.list.add(st);
				}
				splitters.add(newSp);
				if (maxSize == -1) {
					maxSize = newSp.list.size();
					maxBlock = newSp;
				} else if (newSp.list.size() > maxSize) {
					maxSize = newSp.list.size();
					maxBlock = newSp;
				}

				if (node.left != null) {
					queue.add(node.left);
				}
				if (node.right != null) {
					queue.add(node.right);
				}
			}
			block.tree = new SplayTree();
			if (block.list.size() != 0) {
				Block newSp = new Block();
				for (State st : block.list) {
					newSp.list.add(st);
				}
				splitters.add(newSp);
				if (newSp.list.size() > maxSize) {
					maxSize = newSp.list.size();
					maxBlock = newSp;
				}
			}
			splitters.remove(maxBlock);
		}
	}

	public static boolean[][] decide(double[][] probability, int[] label) {
		final int states = label.length;
		Map<Integer, Block> labelToBlock = new HashMap<Integer, Block>(); // maps labels to blocks
		for (int i = 0; i < states; i++) {
			State state = new State(i);
			if (!labelToBlock.containsKey(label[i])) {
				labelToBlock.put(label[i], new Block());
			}
			Block block = labelToBlock.get(label[i]);
			state.block = block;
			block.list.add(state);
		}

		List<Block> partition = new LinkedList<Block>();
		partition.addAll(labelToBlock.values());

		ProbabilisticBisimilarity bis = new ProbabilisticBisimilarity();
		bis.lump(partition, probability);

		boolean[][] bisimilar = new boolean[states][states];
		for (Block block : partition) {
			if (block.list.size() > 1) {
				for (State first : block.list) {
					for (State second : block.list) {
						if (first.index < second.index) {
							bisimilar[first.index][second.index] = true;
						}
					}
				}
			}
		}
		return bisimilar;
	}
}
